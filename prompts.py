from llama_index.core import PromptTemplate

instruction_str = """\
    1. CONSIDER THE PROMPT AS AN ONGOING CONVERSATION
    2. ONLY GIVE AN ANSWER TO THE LAST LINE
    6. EXPRESS PERCENTAGE IN %"""

new_prompt = PromptTemplate(
    """\
    Follow these instructions:
    {instruction_str}"""
)

context = """Purpose: The primary role of this agent is to assist users by providing the most accurate 
            information about the NBA"""