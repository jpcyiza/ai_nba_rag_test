# Import modules
from pathlib import Path
from llama_index.llms.ollama import Ollama
from llama_index.core.embeddings import resolve_embed_model
from llama_index.core import VectorStoreIndex, Settings
from llama_index.readers.file import CSVReader
from llama_index.readers.file import PDFReader

from prompts import new_prompt, instruction_str, context

# Loading Data
nba_players_stats = CSVReader().load_data(
    file=Path("data/nba_23_players_stats.csv"),
    extra_info={
        "description": "CSV about NBA players stats in 2022-2023 season"
    }
)

nba_23_playoffs_pdf = PDFReader().load_data(
    file=Path("data/nba_23_playoffs.pdf"),
    extra_info={
        "description": "PFD about NBA 2022-2023 playoffs"
    }
)

# Configure LLM
Settings.embed_model = resolve_embed_model("local:BAAI/bge-m3")
Settings.llm = Ollama(model="mistral", request_timeout=50.0)

# Create VectorStoreIndex and query engine
store_index = VectorStoreIndex.from_documents(nba_23_playoffs_pdf)
query_engine = store_index.as_query_engine(streaming=True)
query_engine.update_prompts({"ollama_prompt": new_prompt})
prompt_session = ""

print("\033[4mEnter a prompt (/q to quit)\033[0m")



while (prompt := input("\033[1mUser:>> \033[0m")) != "/q":
    print("\n\n\033[1m\033[94m>🤖 AI is thinking...\033[0m\033[0m")
    prompt_session = prompt
    response = query_engine.query(prompt_session)
    response.print_response_stream()
    prompt_session = prompt_session + "\n" + response.response_txt
    print("\n\n")
