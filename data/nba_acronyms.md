
# NBA statistics

| Statistic      | Acronym   | Description                                          |
| -------------- | --------- | ---------------------------------------------------- |
| Points Per Game | PPG       | The average number of points scored per game        |
| Rebounds Per Game | RPG     | The average number of rebounds grabbed per game    |
| Assists Per Game | APG      | The average number of assists provided per game      |
| Field Goal %   | FG%       | The percentage of field goals made during a game    |
| Three-Point %  | 3P%        | The percentage of three-point shots made during a game|
| Effective Field Goal % | eFG%    | An adjusted shooting efficiency that accounts for the value of three-point and free throw attempts |
| True Shooting % | TS%       | A more accurate measure of shooting efficiency        |
| Personal Fouls Per Game | PF/PG  | The average number of personal fouls committed per game|
| Steals Per Game | SPG    | The average number of steals made per game            |
| Blocks Per Game | BPG     | The average number of blocks recorded per game       |
| Plus-Minus | PM      | A measure of a player's impact on the team while on the court |
| Player Efficiency Rating | PER | A single number rating that sums up all aspects of a player’s performance  |
| Win Shares Per 48 Minutes | WS/48 | An estimate of the number of wins a player contributes over an 82-game season. |
